(function($){

    /* HOME SLIDER */
    $('.home__slider').not('.slick-initialized').slick({
        arrows: false,
        dots: true,
        appendDots: $('.home-slider-dots')
    });

    /* INNER SLIDER */
    $('.inner__slider').not('.slick-initialized').slick({
        arrows: false,
        dots: true,
        appendDots: $('.inner-slider-dots')
    });

    /* GALLERY PHOTO SLIDER */
    $('.photo-slider').not('.slick-initialized').slick({
        arrows: true,
        slidesToShow: 3,
          slidesToScroll: 1,
          variableWidth: true,
          prevArrow: $('.photo-slider-prev'),
          nextArrow: $('.photo-slider-next'),
          responsive: [
              {
                  breakpoint: 768,
                  settings: {
                      slidesToShow: 2
                  }
              },

              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 1,
                      variableWidth: false
                  }
              }
          ]
    });

    /* GALLERY VIDEO SLIDER */
    $('.video-slider').not('.slick-initialized').slick({
        arrows: true,
        prevArrow: $('.video-slider-prev'),
        nextArrow: $('.video-slider-next'),
    slidesToShow: 1,
    variableWidth: true,
    centerMode: true,
    responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false
          }
        }
      ]
    });

    /* GALLERY TV SLIDER */
    $('.tv__slider').not('.slick-initialized').slick({
        arrows: true,
        slidesToShow: 3,
          slidesToScroll: 1,
          variableWidth: true,
          prevArrow: $('.tv-slider-prev'),
          nextArrow: $('.tv-slider-next'),
          responsive: [
              {
                  breakpoint: 768,
                  settings: {
                      slidesToShow: 2
                  }
              },

              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 1,
                      variableWidth: false
                  }
              }
          ]
    });

  /* SMTV GALLERY SLIDER */
  $('.tv__main-trip').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.tv-slider-trip-prev'),
      nextArrow: $('.tv-slider-trip-next'),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },

        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false
          }
        }
      ]
  });

  $('.tv__main-cor').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.tv-slider-cor-prev'),
      nextArrow: $('.tv-slider-cor-next'),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },

        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false
          }
        }
      ]
  });

  $('.tv__main-factoid').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.tv-slider-factoid-prev'),
      nextArrow: $('.tv-slider-factoid-next'),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },

        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false
          }
        }
      ]
  });

  $('.library-watch').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.library-watch-prev'),
      nextArrow: $('.library-watch-next'),
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
          }
        },

        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

  $('.library-bmark').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.library-bmark-prev'),
      nextArrow: $('.library-bmark-next'),
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2
          }
        },

        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

  $('.library-redeem').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.library-redeem-prev'),
      nextArrow: $('.library-redeem-next'),
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2
          }
        },

        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

  /* CUSTOM SLIDER */
  $('.custom-slider').not('.slick-initialized').slick({
    arrows: true,
    slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.custom-slider-prev'),
      nextArrow: $('.custom-slider-next'),
      centerMode: true,
      centerPadding: '10px',
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

  /* TAPES SLIDER */
  $('.tapes').not('.slick-initialized').slick({
    arrows: true,
    prevArrow: $('.tapes___nav-prev'),
    nextArrow: $('.tapes___nav-next'),
    slidesToShow: 1,
    swipe: false
  });

  /* RESET FIELD */
  $('.reset-trigger').click(function() {
    var target = $(this).data('field');
    var id = '#' + target;

    $(id).val('');
  });

  /* STICKY KIT */
  var windowWidth = $(window).width();

  if(windowWidth < 768) {
    $('#sidebar').trigger('sticky_kit:detach');
  }
  else {
    $('#sidebar').stick_in_parent({
      offset_top: 10
    });
  }

})(jQuery);

/* SCROLL TO TOP ======================================*/

  window.onscroll = function() {scrollFunction()};

  function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          document.getElementById("scrollTop").style.display = "block";
      } else {
          document.getElementById("scrollTop").style.display = "none";
      }
  }

  function topFunction() {
      document.body.scrollTop = 0; // For Chrome, Safari and Opera
      document.documentElement.scrollTop = 0; // For IE and Firefox
  }

/* SCROLL TO TOP ======================================*/

/* TOGGLE MEMBER MOBILE MENU ==========================*/
function toggleMemberMenu() {
  /* OPEN */
  $('.member-toggle').click(function() {
    $('.member__sidebar').css('right', '0');
  });

  /* CLOSE */
  $('.member__sidebar-close').click(function() {
    $('.member__sidebar').css('right', '100%');
  });
}
/* TOGGLE MEMBER MOBILE MENU ==========================*/

/* toggle password visibility =========================*/
  function togglePassword() {
    var passFlag = 0;

    $('#password + .fa').click(function() {
      var input = $(this).siblings('input');

      if(passFlag == 0) {
        input.attr('type', 'text');

        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');

        console.log('buka');

        passFlag = 1;
      }

      else {

        input.attr('type', 'password');

        $(this).removeClass('fa-eye-slash')
        $(this).addClass('fa-eye');
        console.log('tutup');
        passFlag = 0;
      }
    });
  }

/* SEARCH */
function setSearch() {
  /* open search */
  $('.search-trigger').click(function() {
    $('.search-bar').fadeIn(400);
  });

  /* close search */
  $('.search-close').click(function() {
    $('.search-bar').fadeOut(400);
  });
}

function setEquPlyr() {

  var currentURL = window.location.href;

  /* PLYR & EQUALIZER*/
  
  if(currentURL.indexOf('superband_detail') > -1) {
      $('#song1').equalizer({
          // height / width
          width: 320,
          height: 80,

          // colors
          color: "#0f1957",
          color1: "#a94442",
          color2: "#f2b400",

          // the number of bars
          bars: 15,

          // margin between bars
          barMargin: 0.5,

          // the number of components in one bar
          components: 10,

          // margin between components
          componentMargin: 0.5,

          // rate of equalizer frequency
          frequency: 10,

          // refresh time of animation
          refreshTime: 100
      });
  }
  else {
    plyr.setup();
  }
}

$(document).ready(function() {
  toggleMemberMenu();
  togglePassword();
  setSearch();
  setEquPlyr();
});
